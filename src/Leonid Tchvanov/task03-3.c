#include <stdio.h>

int main()
{
    unsigned int charFrequency[127];
    int i, a;
    for (i=0; i<127; i++)
        charFrequency[i]=0;
    while ((a=_getch())!='\r')
    {
        putchar(a);
        charFrequency[a]++;
    }
    putchar('\n');
    for (i=0; i<127; i++)
    {
        if (charFrequency[i]>0)
            printf("%c-%d,", i, charFrequency[i]);
    }
    printf("\b \b");
    return 0;
}
