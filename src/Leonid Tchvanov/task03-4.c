#define MAX_RANDOM_NUMBER 1000
#define N 100
#include <stdio.h>
#include <time.h>

int maxElement(int *arr, int length)
{
    int max=0, i;
    for (i=0; i<length; i++)
    {
        if (arr[i]>max)
            max = arr[i];
    }
    return max;
}

int minElement(int *arr, int length)
{
    int min=MAX_RANDOM_NUMBER+1, i;
    for (i=0; i<length; i++)
    {
        if (arr[i]<min)
            min = arr[i];
    }
    return min;
}

float averageElement(int *arr, int length)
{
    int i, sum=0;
    for (i=0; i<length; i++)
    {
        sum+=arr[i];
    }
    return (float)sum/length;
}

int main()
{
    int arr[N];
    int i;
    srand(time(0));
    for (i=0; i<N; i++)
    {
        arr[i] = rand()%MAX_RANDOM_NUMBER;
    }
    printf("Maximum element - %d\nMinimum element - %d\nAverage value - %.2f", maxElement(&arr, N), minElement(&arr, N), averageElement(&arr, N));
    return 0;
}
